// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers','starter.services', 'ui.router'])

.run(function($ionicPlatform, $cordovaSQLite, $cordovaStatusbar, $rootScope, $state, $stateParams, $cordovaNetwork, $cordovaInAppBrowser) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    db = window.openDatabase("estufa.db", "", "Estufa", 30 * 1024 * 1024);
    // $cordovaSQLite.execute(db, "DROP TABLE config");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS config (id INTEGER primary key, config TEXT NOT NULl, value TEXT NOT NULL, date_time TEXT NOT NULL)");
    // $cordovaSQLite.execute(db, "DROP TABLE user");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS user (id INTEGER primary key, name TEXT NOT NULL, cpf TEXT NOT NULL, email TEXT NOT NULL, sex TINYINT, cell TEXT, image TEXT, password TEXT, birth TEXT, profession TEXT, cep TEXT, date_time TEXT NOT NULL)");
    // $cordovaSQLite.execute(db, "DROP TABLE device");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS device (id INTEGER primary key, user_id INTEGER NOT NULL, name TEXT NOT NULL, waterTimer INTEGER NOT NULL, lightTimer INTEGER NOT NULL, lightDuration INTEGER NOT NULL, temperature INTEGER NOT NULL,email TEXT NOT NULL, sex TINYINT, cell TEXT, image TEXT, password TEXT, block TINYINT, birth TEXT, profession TEXT, cep TEXT, date_time TEXT NOT NULL, FOREIGN KEY(client_id) REFERENCES client(id))");


    // $rootScope.$urlWebsite = 'http://localhost:3000';
    $rootScope.$urlWebsite = 'http://estufa.herokuapp.com';

    // var type = $cordovaNetwork.getNetwork();

    // var isOnline = $cordovaNetwork.isOnline()

    // var isOffline = $cordovaNetwork.isOffline()

    // console.log(JSON.stringify(window.navigator));
  });
})

.config(function($stateProvider, $urlRouterProvider, $cordovaInAppBrowserProvider) {
  

  var defaultOptions = {
    location: 'no',
    clearcache: 'no',
    toolbar: 'no'
  };

  document.addEventListener("deviceready", function () {

    $cordovaInAppBrowserProvider.setDefaultOptions(options)

  }, false);

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'homeCtrl'
        }
      }
    })

    .state('app.wifiConfigure', {
      url: '/wifiConfigure',
      views: {
        'menuContent': {
          templateUrl: 'templates/wifiConfigure.html',
          controller: 'wifiConfigureCtrl'
        }
      }
    }) 

    .state('app.validate', {
      url: '/validate',
      views: {
        'menuContent': {
          templateUrl: 'templates/validate.html',
          controller: 'ValidateCtrl'
        }
      }
    })

    .state('app.devices', {
      url: '/devices',
      views: {
        'menuContent': {
          templateUrl: 'templates/devices.html',
          controller: 'DevicesCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/device/:device',
    views: {
      'menuContent': {
        templateUrl: 'templates/device.html',
        controller: 'ChildCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
