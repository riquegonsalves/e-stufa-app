angular.module('starter.services', [])

.factory('DBA', function($cordovaSQLite, $q, $ionicPlatform, $http) {
  var self = this;

  // Handle query's and potential errors
  self.query = function (query, parameters) {
    parameters = parameters || [];
    var q = $q.defer();

    $ionicPlatform.ready(function () {
      $cordovaSQLite.execute(db, query, parameters)
        .then(function (result) {
          q.resolve(result);
        }, function (error) {
          console.warn('I found an error');
          console.warn(JSON.stringify(error));
          q.reject(error);
        });
    });
    return q.promise;
  }

  // Proces a result set
  self.getAll = function(result) {

    var output = [];

    for (var i = 0; i < result.rows.length; i++) {
      output.push(result.rows.item(i));
    }
      // console.log(JSON.stringify(output[0]));
    return output;
  }

  // Proces a single result
  self.getById = function(result) {
    var output = null;
    output = angular.copy(result.rows.item(0));
    return output;
  }


  self.synchronizeData = function(url,factoryObject){

    if(url == '' || url == undefined){
      return false;
    }

    if(factoryObject == '' || factoryObject == undefined){
      return false;
    }


    /*Sincronia de dados : inicio */
    return $http({
      method: 'post',
      url : url,
    }).then(function successCallback(response) {

          // console.log('RESPOSTA synchronizeData');
          // console.log(JSON.stringify(response.data));

        //a requisição obteve sucesso?
        if(response.data.success){
          //percorrendo os registros que foram enviados
          // console.log(url);
          response.data.message.forEach(function(data) {
         
            //verifica se ja existe o registro
            var registry = factoryObject.existenceRegistry(data.id).then(function(res){
              var result = null;
              if(res.quantity == 1){
                //atualizar registro
                var update = factoryObject.update(data);
              } else {
                //criar o registro
                var save = factoryObject.add(data);
              }
            }); //existenceRegistry:end
          }); //forEach:end
          
          //removendo os dados que não existem mais na aplicação
          if(response.data.remove != undefined){
            response.data.remove.forEach(function(data){
              //verifica se ja existe o registro
              var registry = factoryObject.existenceRegistry(data.id).then(function(res){
                if(res.quantity == 1){
                  factoryObject.remove(data.id).then(function(resRemove){
                    //registro excluido com sucesso?
                  });
                }//resultado == 1
              }); //existenceRegistry:end
            });//forEach
          }//existem dados para serem removidos?

        } else if(response.data.error){
          return false
        }//if data.success:end
        return true;

    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.log('erro');
      console.log(response);
      return false;
    });//end http action
    // /*Sincronia de dados : fim */

  }

  return self;
})


.factory('Configs', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM config")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(configId) {
    var parameters = [configId];
    return DBA.query("SELECT id, config, value, date_time FROM config WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(config) {
    var parameters = [config.id, config.config, config.value, config.date_time];
    return DBA.query("INSERT INTO config (id, config, value, date_time) VALUES (?,?,?,?)", parameters);
  }

  self.addNotId = function(config) {
    var parameters = [config.config, config.value, config.date_time];
    return DBA.query("INSERT INTO config (config, value, date_time) VALUES (?,?,?)", parameters);
  }

  self.update = function(config) {
    var parameters = [condif.id, config.config, config.value, config.date_time];
    return DBA.query("UPDATE config SET id = (?), config = (?), value = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.existenceRegistry = function(configId) {
    var parameters = [condigId];
    return DBA.query("SELECT COUNT(*) as quantity FROM config WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from config ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.getAccess = function() {
    return DBA.query("SELECT id, config, value, date_time FROM config WHERE config = 'access' ORDER BY date_time DESC LIMIT 1 ")
      .then(function(result) {
        if(result.rows.length > 0){
          var output = null;
          output = angular.copy(result.rows.item(0));
          return output;
        } else {
          return null;
        }
      });
  }


  return self;
})



.factory('Enterprises', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM enterprise ORDER BY fantasy_name")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(enterpriseId) {
    var parameters = [enterpriseId];
    return DBA.query("SELECT id, fantasy_name, description, logo FROM enterprise WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(enterprise) {
    var parameters = [ enterprise.id, enterprise.cnpj, enterprise.fantasy_name, enterprise.description, enterprise.logo, enterprise.date_time];
    return DBA.query("INSERT INTO enterprise (id, cnpj, fantasy_name, description, logo, date_time) VALUES (?,?,?,?,?,?)", parameters);
  }

  self.update = function(enterprise) {
    var parameters = [enterprise.id, enterprise.cnpj, enterprise.fantasy_name, enterprise.description, enterprise.logo, enterprise.date_time, enterprise.id];
    return DBA.query("UPDATE enterprise SET id = (?), cnpj = (?), fantasy_name = (?), description = (?), logo = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(enterpriseId){
    var parameters = [enterpriseId];
    return DBA.query("DELETE FROM enterprise where id = (?)",parameters);
  }

  self.existenceRegistry = function(enterpriseId) {
    var parameters = [enterpriseId];
    return DBA.query("SELECT COUNT(*) as quantity FROM enterprise WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from enterprise ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }

  return self;
})


.factory('Clients', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM client ORDER BY fantasy_name")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(clientId) {
    var parameters = [clientId];
    return DBA.query("SELECT id, fantasy_name, description, logo, color FROM client WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(client) {
    var parameters = [ client.id, client.cnpj, client.fantasy_name, client.description, client.logo, client.date_time, client.color];
    return DBA.query("INSERT INTO client (id, cnpj, fantasy_name, description, logo, date_time, color) VALUES (?,?,?,?,?,?,?)", parameters);
  }

  self.update = function(client) {
    console.log('AQUI');
    // console.log(JSON.stringify(client));
    var parameters = [client.id, client.cnpj, client.fantasy_name, client.description, client.logo, client.date_time, client.color, client.id];
    return DBA.query("UPDATE client SET id = (?), cnpj = (?), fantasy_name = (?), description = (?), logo = (?), date_time = (?), color = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(clientId){
    var parameters = [clientId];
    return DBA.query("DELETE FROM client where id = (?)",parameters);
  }

  self.existenceRegistry = function(clientId) {
    var parameters = [clientId];
    return DBA.query("SELECT COUNT(*) as quantity FROM client WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from client ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }

  return self;
})


.factory('Units', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM unit")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(unitId) {
    var parameters = [unitId];
    return DBA.query("SELECT * FROM unit WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(unit) {
    var parameters = [ unit.id, unit.enterprise, unit.name, unit.cep, unit.address, unit.phone, unit.buy_code, unit.date_time];
    return DBA.query("INSERT INTO unit (id, enterprise_id, name, cep, address, phone, buy_code, date_time) VALUES (?,?,?,?,?,?,?,?)", parameters);
  }

  self.update = function(unit) {
    var parameters = [unit.id, unit.enterprise, unit.name, unit.cep, unit.address, unit.phone, unit.buy_code, unit.date_time, unit.id];
    return DBA.query("UPDATE unit SET id = (?), enterprise_id = (?), name = (?), cep = (?), address = (?), phone = (?), buy_code = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(unitId){
    var parameters = [unitId];
    return DBA.query("DELETE FROM unit where id = (?)",parameters);
  }

  self.existenceRegistry = function(unitId) {
    var parameters = [unitId];
    return DBA.query("SELECT COUNT(*) as quantity FROM unit WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from unit ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.getAllUnit = function(){
    return DBA.query("SELECT *, u.id as unit_id FROM unit as u INNER JOIN enterprise as e where u.enterprise_id = e.id ")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.getAllUnitByIdEnterprise = function(enterpriseId){
    var parameters = [enterpriseId];
    return DBA.query("SELECT * FROM unit WHERE enterprise_id = ? ",parameters)
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }

  
  return self;
})




.factory('Promotions', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    
    return DBA.query("SELECT * FROM promotion")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(promotionId) {
    var parameters = [promotionId];
    //return DBA.query("SELECT * FROM promotion WHERE id = (?)", parameters)
    return DBA.query("SELECT p.*, e.fantasy_name as enterprise_name, p.name as promotion_name, u.name as unit_name FROM promotion as p LEFT JOIN enterprise as e ON p.enterprise_id = e.id LEFT JOIN unit as u ON p.unit_id = u.id WHERE p.id = (?)",parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(promotion) {
    var parameters = [ promotion.id, promotion.enterprise, promotion.unit, promotion.name, promotion.code, promotion.image, promotion.thumb, promotion.description, promotion.float_price, promotion.date_time_start, promotion.date_time_end, promotion.exclusive, promotion.date_time];
    return DBA.query("INSERT INTO promotion (id, enterprise_id, unit_id, name, code, image, thumb, description, price, start_date, end_date, exclusive,date_time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
  }

  self.update = function(promotion) {
    var parameters = [promotion.id, promotion.enterprise, promotion.unit, promotion.name, promotion.code, promotion.image, promotion.thumb, promotion.description, promotion.float_price, promotion.date_time_start, promotion.date_time_end, promotion.exclusive, promotion.date_time, promotion.id];
    return DBA.query("UPDATE promotion SET id = (?), enterprise_id = (?), unit_id = (?), name = (?), code = (?), image = (?), thumb = (?), description = (?), price = (?), start_date = (?), end_date = (?), exclusive = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(promotionId){
    var parameters = [promotionId];
    return DBA.query("DELETE FROM promotion where id = (?)",parameters);
  }

  self.existenceRegistry = function(promotionId) {
    var parameters = [promotionId];
    return DBA.query("SELECT COUNT(*) as quantity FROM promotion WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * FROM promotion ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.getPromotionList = function(){
    //return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p INNER JOIN enterprise as e ON p.enterprise_id = e.id ORDER BY e.fantasy_name")
    //return DBA.query("SELECT * FROM promotion")
    //TODO: não compreendi o que ocorreu, o inner join parou de funcionar, ele funciona apenas com left join
    return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, p.exclusive, e.id as enterprise_id, e.fantasy_name as enterprise_name FROM promotion as p LEFT JOIN enterprise as e ON p.enterprise_id = e.id ORDER BY e.fantasy_name")
      .then(function(result){
        return DBA.getAll(result);
      });
  } 

  self.getPromotionListClient = function(promotionIds){
    //return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p INNER JOIN enterprise as e ON p.enterprise_id = e.id ORDER BY e.fantasy_name")
    //return DBA.query("SELECT * FROM promotion")
    //TODO: não compreendi o que ocorreu, o inner join parou de funcionar, ele funciona apenas com left join

    console.debug("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, p.exclusive, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p LEFT JOIN enterprise as e ON p.enterprise_id = e.id ORDER BY e.fantasy_name WHERE p.id IN (" + promotionIds.join() + ")");
    return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, p.exclusive, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p LEFT JOIN enterprise as e ON p.enterprise_id = e.id ORDER BY e.fantasy_name WHERE p.id IN (" + promotionIds.join() + ")")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.getPromotionListSearch = function(stringSearch){
    // var parameters = [stringSearch]
    // return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.description, p.price, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p INNER JOIN enterprise as e ON p.enterprise_id = e.id WHERE p.name LIKE '%(?)%' OR e.fantasy_name LIKE '%(?)%' OR p.description LIKE '%(?)%' ORDER BY e.fantasy_name",parameters)
    var pesquisa = 'SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, p.exclusive, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p INNER JOIN enterprise as e ON p.enterprise_id = e.id WHERE p.name LIKE "%'+stringSearch+'%" OR e.fantasy_name LIKE "%'+stringSearch+'%" ORDER BY e.fantasy_name';
    return DBA.query(pesquisa)
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.getPromotionListExclusive = function(){
    return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, p.exclusive, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p INNER JOIN enterprise as e ON p.enterprise_id = e.id WHERE p.exclusive = 1 ORDER BY e.fantasy_name ")
    //return DBA.query("SELECT * FROM promotion")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

    self.getPromotionListByEnterprise = function(enterpriseId){
      var parameters = [enterpriseId];
    return DBA.query("SELECT p.id, p.name as promotion_name, p.image, p.thumb, p.description, p.price, p.exclusive, e.id as enterprise_id, e.fantasy_name as enterprise_name  FROM promotion as p INNER JOIN enterprise as e ON p.enterprise_id = e.id WHERE p.enterprise_id = (?) ORDER BY e.fantasy_name", parameters)
    //return DBA.query("SELECT * FROM promotion")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }
  
  return self;
})



.factory('Plans', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM plan")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(planId) {
    var parameters = [planId];
    return DBA.query("SELECT * FROM plan WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(plan) {
    var parameters = [ plan.id, plan.name, plan.description, plan.float_price, plan.number_installment, plan.image, plan.date_time];
    return DBA.query("INSERT INTO plan (id, name, description, price, number_installment, image, date_time) VALUES (?,?,?,?,?,?,?)", parameters);
  }

  self.update = function(plan) {
    var parameters = [plan.id, plan.name, plan.description, plan.float_price, plan.number_installment, plan.image, plan.date_time, plan.id];
    return DBA.query("UPDATE plan SET id = (?), name = (?), description = (?), price = (?), number_installment = (?), image = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(planId){
    var parameters = [planId];
    return DBA.query("DELETE FROM plan where id = (?)",parameters);
  }

  self.existenceRegistry = function(planId) {
    var parameters = [planId];
    return DBA.query("SELECT COUNT(*) as quantity FROM plan WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * FROM plan ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }

  return self;
})



.factory('Questions', function($cordovaSQLite, DBA)  {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM question")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(questionId) {
    var parameters = [questionId];
    return DBA.query("SELECT * FROM question WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(question) {
    var parameters = [ question.id, question.question, question.answer, question.date_time];
    return DBA.query("INSERT INTO question (id, question, answer, date_time) VALUES (?,?,?,?)", parameters);
  }

  self.update = function(question) {
    var parameters = [question.id, question.question, question.answer, question.date_time, question.id];
    return DBA.query("UPDATE question SET id = (?), question = (?), answer = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(questionId){
    var parameters = [questionId];
    return DBA.query("DELETE FROM question where id = (?)",parameters);
  }

  self.existenceRegistry = function(questionId) {
    var parameters = [questionId];
    return DBA.query("SELECT COUNT(*) as quantity FROM question WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * FROM question ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }

  return self;

  
})



.factory('Users', function($cordovaSQLite, DBA)  {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM user")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(userId) {
    var parameters = [userId];
    return DBA.query("SELECT * FROM user WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(user) {
    var parameters = [user.id, user.name, user.cpf, user.email, user.sex, user.cell, user.password, user.image, user.block, user.birth, user.profession, user.cep, user.date_time, user.client];
    return DBA.query("INSERT INTO user (id, name, cpf, email, sex, cell, password, image, block, birth, profession, cep, date_time, client_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
  }

  self.update = function(user) {
    var parameters = [user.id, user.name, user.cpf, user.email, user.sex, user.cell, user.password, user.image, user.block, user.birth, user.profession, user.cep, user.date_time, user.client, user.id];
    return DBA.query("UPDATE user SET id = (?), name = (?), cpf = (?), email = (?), sex = (?), cell = (?), password = (?), image = (?), block = (?), birth = (?), profession = (?), cep = (?), date_time = (?), client_id = (?) WHERE id = (?)", parameters);
  }

  self.existenceRegistry = function(userId) {
    var parameters = [userId];
    return DBA.query("SELECT COUNT(*) as quantity FROM user WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * FROM user ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  return self;

  
})


.factory('Launchs', function($cordovaSQLite, DBA)  {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM launch")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(launchId) {
    var parameters = [launchId];
    return DBA.query("SELECT * FROM launch WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.getAllById = function(launchId){
    var parameters = [launchId];
    // console.log('LAUNCH ID: '+ launchId);
    //return DBA.query("SELECT *, l.id as launch_id, us.name as user_name, us.image as user_image, u.name as unit_name, e.logo as enterprise_image, p.image as promotion_image FROM launch as l INNER JOIN unit as u ON l.unit_id = u.id INNER JOIN enterprise as e ON u.enterprise_id = e.id INNER JOIN user as us ON l.user_id = us.id INNER JOIN promotion as p ON l.promotion_id = p.id WHERE l.id = (?)", parameters)
    return DBA.query("SELECT launch.id as launch_id, launch.date_time as launch_date_time, user.name as user_name, user.image as user_image, unit.name as unit_name, enterprise.logo as enterprise_image, promotion.image as promotion_image, enterprise.fantasy_name as enterprise_name, promotion.name as promotion_name FROM launch LEFT JOIN unit ON launch.unit_id = unit.id LEFT JOIN user ON launch.user_id = user.id LEFT JOIN promotion ON launch.promotion_id = promotion.id LEFT JOIN enterprise ON promotion.enterprise_id = enterprise.id WHERE launch.id = (?)", parameters)
    .then(function(result){
      // console.log('RESULT: '+ JSON.stringify(result.rows.item(0)));

      var output = null;
      
        output = angular.copy(result.rows.item(0));
     
      return output;
    });
  }

  self.add = function(launch) {
    var parameters = [launch.id, launch.unit_id, launch.card_id, launch.user_id, launch.promotion_id, launch.launch_status_id, launch.total_price, launch.date_time];
    return DBA.query("INSERT INTO launch (id, unit_id, card_id, user_id, promotion_id, launch_status_id, total_price, date_time) VALUES (?,?,?,?,?,?,?,?)", parameters);
  }

  self.update = function(launch) {
    var parameters = [launch.id, launch.unit_id, launch.card_id, launch.user_id, launch.promotion_id, launch.launch_status_id, launch.total_price, launch.date_time, launch.id];
    return DBA.query("UPDATE launch SET id = (?), unit_id = (?), card_id = (?), user_id = (?), promotion_id = (?), launch_status_id = (?), total_price = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.existenceRegistry = function(launchId) {
    var parameters = [launchId];
    return DBA.query("SELECT COUNT(*) as quantity FROM launch WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * FROM launch ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  return self;

  
})




.factory('Categories', function($cordovaSQLite, DBA) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM category ORDER BY name")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.get = function(categoryId) {
    var parameters = [categoryId];
    return DBA.query("SELECT id, image, name FROM category WHERE id = (?)", parameters)
      .then(function(result) {
        return DBA.getById(result);
      });
  }

  self.add = function(category) {

    var parameters = [ category.id, category.image, category.name, category.date_time];
    return DBA.query("INSERT INTO category (id, image, name, date_time) VALUES (?,?,?,?)", parameters);
  }

  self.update = function(category) {
    var parameters = [ category.id, category.image, category.name, category.date_time, category.id];
    return DBA.query("UPDATE category SET id = (?), image = (?), name = (?), date_time = (?) WHERE id = (?)", parameters);
  }

  self.remove = function(categoryId){
    var parameters = [categoryId];
    return DBA.query("DELETE FROM category where id = (?)",parameters);
  }

  self.existenceRegistry = function(categoryId) {
    var parameters = [categoryId];
    return DBA.query("SELECT COUNT(*) as quantity FROM category WHERE id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from category ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url, factoryObject){
    return DBA.synchronizeData(url, factoryObject);
  }

  return self;
})




.factory('PromotionCategories', function($cordovaSQLite, DBA,$http) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM promotion_category ORDER BY name")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.getPromotionByCategory = function(categoryId) {
    var parameters = [categoryId];
    return DBA.query("SELECT promotion_id FROM promotion_category WHERE category_id = (?) GROUP BY promotion_id", parameters)
      .then(function(result) {
        return DBA.getAll(result);
      });
  }

  self.getCategoryByPromotion = function(promotionId) {
    var parameters = [promotionId];
    return DBA.query("SELECT category_id, FROM promotion_category WHERE promotion_id = (?) GROUP BY category_id", parameters)
      .then(function(result) {
        return DBA.getAll(result);
      });
  }

  self.add = function(promotion_category) {
    var parameters = [ promotion_category.promotion_id, promotion_category.category_id, promotion_category.date_time];
    return DBA.query("INSERT INTO promotion_category (promotion_id, category_id, date_time) VALUES (?,?,?)", parameters);
  }

  self.update = function(promotion_category) {
    var parameters = [ promotion_category.promotion_id, promotion_category.category_id, promotion_category.date_time, promotion_category.promotion_id, promotion_category.category_id];
    return DBA.query("UPDATE promotion_category SET promotion_id = (?), category_id = (?), date_time = (?) WHERE promotion_id = (?) AND category_id = (?)", parameters);
  }

  self.remove = function(promotion_category){
    var parameters = [promotion_category.promotion_id,promotion_category.category_id];
    return DBA.query("DELETE FROM promotion_category where promotion_id = (?) AND category_id = (?)",parameters);
  }

  self.existenceRegistry = function(promotion_category) {
    var parameters = [promotion_category.promotion_id, promotion_category.category_id];
    return DBA.query("SELECT COUNT(*) as quantity FROM promotion_category WHERE promotion_id = (?) AND category_id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from promotion_category ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url){

    if(url == '' || url == undefined){
      return false;
    }

    /*Sincronia de dados : inicio */
    return $http({
      method: 'post',
      url : url,
    }).then(function successCallback(response) {
        //a requisição obteve sucesso?
        if(response.data.success){
          //percorrendo os registros que foram enviados
          response.data.message.forEach(function(promotion_category) {
            //verifica se ja existe o registro
            var registry = self.existenceRegistry(promotion_category).then(function(res){
              var result = null;
              if(res.quantity == 1){
                //atualizar registro
                var update = self.update(promotion_category);
              } else {
                //criar o registro
                var save = self.add(promotion_category);
              }
            }); //existenceRegistry:end
          }); //forEach:end
          
          //removendo os dados que não existem mais na aplicação
          if(response.data.remove != undefined){
            response.data.remove.forEach(function(promotion_category){
              //verifica se ja existe o registro
              var registry = self.existenceRegistry(promotion_category).then(function(res){
                if(res.quantity == 1){
                  self.remove(promotion_category).then(function(resRemove){
                    //registro excluido com sucesso?
                  });
                }//resultado == 1
              }); //existenceRegistry:end
            });//forEach
          }//existem dados para serem removidos?

        } else if(response.data.error){
          return false
        }//if data.success:end
        return true;

    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      // console.log('erro');
      // console.log(response);
      return false;
    });//end http action
    // /*Sincronia de dados : fim */


    //return DBA.synchronizeData(url, factoryObject);
  }

  return self;
})











/*
.factory('Enterprises', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var enterprises = 
  [{ 
    id: 1,
    name: 'Empresa 1',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png',
    address : 'Asa norte',
    phone: '(61) 1111-1111'
  }, { 
    id: 2,
    name: 'Empresa 2',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png',
    address : 'Asa sul',
    phone: '(61) 2222-2222'
  }, { 
    id: 3,
    name: 'Empresa 3',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png',
    address : 'Taguatinha',
    phone: '(61) 3333-3333'
  }, { 
    id: 4,
    name: 'Empresa 4',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg',
    address : 'Sudoeste',
    phone: '(61) 7777-7777'
  }, { 
    id: 5,
    name: 'Empresa 5',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460',
    address : 'Lago norte',
    phone: '(61) 9999-9999'
  }];

  

  return {
    all: function() {
      return enterprises;
    },
    remove: function(enterprise) {
      enterprises.splice(enterprises.indexOf(enterprise), 1);
    },
    get: function(enterpriseId) {
      for (var i = 0; i < enterprises.length; i++) {
        if (enterprises[i].id === parseInt(enterpriseId)) {
          return enterprises[i];
        }
      }
      return null;
    }
  };


})
*/

.factory('PromotionClients', function($cordovaSQLite, DBA,$http) {
  var self = this;

  self.all = function() {
    return DBA.query("SELECT * FROM promotion_client")
      .then(function(result){
        return DBA.getAll(result);
      });
  }

  self.getPromotionByClient = function(clientId) {
   

    var parameters = [clientId];
    return DBA.query("SELECT promotion.name, promotion.id, promotion.name as promotion_name, promotion.image, promotion.thumb, promotion.description, promotion.price, promotion.exclusive, enterprise.id as enterprise_id, enterprise.fantasy_name as enterprise_name FROM promotion_client LEFT JOIN promotion ON promotion_client.promotion_id = promotion.id LEFT JOIN enterprise ON promotion.enterprise_id = enterprise.id WHERE client_id = (?)", parameters)
      .then(function(result) {
        
        return DBA.getAll(result);
      });
  }

  self.getClientByPromotion = function(promotionId) {
    var parameters = [promotionId];
    return DBA.query("SELECT client_id, FROM promotion_client WHERE promotion_id = (?) GROUP BY client_id", parameters)
      .then(function(result) {
        return DBA.getAll(result);
      });
  }

  self.add = function(promotion_client) {
    var parameters = [ promotion_client.promotion_id, promotion_client.client_id, promotion_client.date_time];
    return DBA.query("INSERT INTO promotion_client (promotion_id, client_id, date_time) VALUES (?,?,?)", parameters);
  }

  self.update = function(promotion_client) {
    var parameters = [ promotion_client.promotion_id, promotion_client.client_id, promotion_client.date_time, promotion_client.promotion_id, promotion_client.client_id];
    return DBA.query("UPDATE promotion_client SET promotion_id = (?), client_id = (?), date_time = (?) WHERE promotion_id = (?) AND client_id = (?)", parameters);
  }

  self.remove = function(promotion_client){
    var parameters = [promotion_client.promotion_id,promotion_client.client_id];
    return DBA.query("DELETE FROM promotion_client where promotion_id = (?) AND client_id = (?)",parameters);
  }

  self.existenceRegistry = function(promotion_client) {
    var parameters = [promotion_client.promotion_id, promotion_client.client_id];
    return DBA.query("SELECT COUNT(*) as quantity FROM promotion_client WHERE promotion_id = (?) AND client_id = (?)", parameters)
      .then(function(result) {
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      });
  }

  self.getLastUpdate = function(){
    return DBA.query("SELECT * from promotion_client ORDER BY date_time DESC limit 1").then(function(result){
      if(result.rows.length > 0){
        var output = null;
        output = angular.copy(result.rows.item(0));
        return output;
      } else {
        return 0;
      }
    });
  }

  self.synchronizeData = function(url){

    if(url == '' || url == undefined){
      return false;
    }

    /*Sincronia de dados : inicio */
    return $http({
      method: 'post',
      url : url,
    }).then(function successCallback(response) {
        //a requisição obteve sucesso?
        if(response.data.success){
          //percorrendo os registros que foram enviados
          response.data.message.forEach(function(promotion_client) {
            //verifica se ja existe o registro
            var registry = self.existenceRegistry(promotion_client).then(function(res){
              var result = null;
              if(res.quantity == 1){
                //atualizar registro
                var update = self.update(promotion_client);
              } else {
                //criar o registro
                var save = self.add(promotion_client);
              }
            }); //existenceRegistry:end
          }); //forEach:end
          
          //removendo os dados que não existem mais na aplicação
          if(response.data.remove != undefined){
            response.data.remove.forEach(function(promotion_client){
              //verifica se ja existe o registro
              var registry = self.existenceRegistry(promotion_client).then(function(res){
                if(res.quantity == 1){
                  self.remove(promotion_client).then(function(resRemove){
                    //registro excluido com sucesso?
                  });
                }//resultado == 1
              }); //existenceRegistry:end
            });//forEach
          }//existem dados para serem removidos?

        } else if(response.data.error){
          return false
        }//if data.success:end
        return true;

    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      // console.log('erro');
      // console.log(response);
      return false;
    });//end http action
    // /*Sincronia de dados : fim */


    //return DBA.synchronizeData(url, factoryObject);
  }

  return self;

})












.factory('Advantages', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var advantages = 
  [{ 
    id: 1,
    enterprise_id : 1,
    enterprise_name: 'Empresa 1',
    name: 'Promoção 1',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    price:'21,00',
    image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png',
    exclusive: true
  }, { 
    id: 2,
    enterprise_id : 2,
    enterprise_name: 'Empresa 2',
    name: 'Promoção 2',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    price:'22,00',
    image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png',
    exclusive: false
  }, { 
    id: 3,
    enterprise_id : 1,
    enterprise_name: 'Empresa 1',
    name: 'Promoção 3',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    price:'33,00',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png',
    exclusive: false
  }, { 
    id: 4,
    enterprise_id : 3,
    enterprise_name: 'Empresa 3',
    name: 'Promoção 4',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    price:'44,00',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg',
    exclusive: true
  }, { 
    id: 5,
    enterprise_id : 1,
    enterprise_name: 'Empresa 1',
    name: 'Promoção 5',
    description: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.',
    price:'55,00',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460',
    exclusive: true
  }];

  

  return {
    all: function() {
      return advantages;
    },
    remove: function(advantage) {
      advantages.splice(advantages.indexOf(advantage), 1);
    },
    get: function(advantageId) {
      for (var i = 0; i < advantages.length; i++) {
        if (advantages[i].id === parseInt(advantageId)) {
          return advantages[i];
        }
      }
      return null;
    },
    exclusive: function(){
      var exclusiveAdvantages = [];
      for (var i = 0; i < advantages.length; i++) {
        if (advantages[i].exclusive) {
          exclusiveAdvantages.push(advantages[i]);
        }
      }
      if(exclusiveAdvantages.length > 0){
        return exclusiveAdvantages;
      }
      return null;
    }
  };

})




/*
.factory('Plans', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var plans = 
  [{ 
    id: 1,
    name: 'Plano Simples',
    itens: [{
      name : 'Vantagem 1',
      name : 'Vantagem 2',
      name : 'Vantagem 3'
    }],
    price:'21,00',
    image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, { 
    id: 2,
    name: 'Plano Casal',
    itens: [{
      name : 'Vantagem 1',
      name : 'Vantagem 2',
      name : 'Vantagem 3'
    }],
    price:'50,00',
    image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, { 
    id: 3,
    name: 'Plano Família',
    itens: [{
      name : 'Vantagem 1',
      name : 'Vantagem 2',
      name : 'Vantagem 3'
    }],
    price:'80,00',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
  }];

  

  return {
    all: function() {
      return plans;
    },
    remove: function(plan) {
      plans.splice(plans.indexOf(plan), 1);
    },
    get: function(planId) {
      for (var i = 0; i < plans.length; i++) {
        if (plans[i].id === parseInt(planId)) {
          return plans[i];
        }
      }
      return null;
    }
  };
})*/

/*.factory('CommonQuestions', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var commonQuestions = 
  [{ 
    id: 1,
    question : "Pergunta 1",
    answer: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.'
  }, { 
    id: 2,
    question : "Pergunta 2",
    answer: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.'
  }, { 
    id: 3,
    question : "Pergunta 3",
    answer: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.'
  }, { 
    id: 4,
    question : "Pergunta 4",
    answer: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.'
  }, { 
    id: 5,
    question : "Pergunta 5",
    answer: 'Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.'
  }];

  

  return {
    all: function() {
      return commonQuestions;
    },
    remove: function(commonQuestion) {
      commonQuestions.splice(commonQuestions.indexOf(commonQuestion), 1);
    },
    get: function(commonQuestionId) {
      for (var i = 0; i < commonQuestions.length; i++) {
        if (commonQuestions[i].id === parseInt(commonQuestionId)) {
          return commonQuestions[i];
        }
      }
      return null;
    }
  };
})*/



;