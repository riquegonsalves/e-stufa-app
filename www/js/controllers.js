angular.module('starter.controllers', [])



.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http, $rootScope, $state, $cordovaNetwork, $ionicPlatform) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // $http({
  //   url: '192.168.4.1', 
  //   method: "GET"
  //  }).then(function successCallback(response) {
  //   console.debug(JSON.stringify(response));

  //   // if(response.data.success){
  //   //   console.debug(JSON.stringify(response.data.user));
  //   //   $scope.user = response.data.user;
  //   //   window.localStorage['authentication'] = JSON.stringify(response.data.user);
  //   //   console.log(window.localStorage['authentication']);
  //   //   $scope.isLogged = true;
  //   //   $timeout(function() {
  //   //     $scope.modalLoading.hide();
  //   //     $scope.closeLogin();
  //   //   }, 1000);
  //   // } else {
  //   //   $scope.messageLogin = response.data.message;
  //   //   $scope.successLogin = response.data.success;
  //   // }
  // });

  // Form data for the login modal
  $scope.loginData = {};
  $scope.signUpData = {};

  

  // console.log(JSON.stringify($cordovaNetwork));

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $ionicModal.fromTemplateUrl('templates/loading.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalLoading = modal;
  });  

  $ionicModal.fromTemplateUrl('templates/signup.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalSignup = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();

  };

  $scope.closeSignUp = function() {
    $scope.modalSignup.hide();

  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.logout = function(){
    $scope.isLogged = false;
    window.localStorage['authentication'] = '';
    $state.go('app.home');

  };

  $scope.signup = function(){
    $scope.modalSignup.show();
  };

  


  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    $scope.modalLoading.show();

    console.log(JSON.stringify($scope.loginData));
    var email = $scope.loginData.username;
    var password = $scope.loginData.password;

    console.log($rootScope.$urlWebsite);
    $http({
      url: $rootScope.$urlWebsite+'/api/auth/'+email+'/'+password+'/', 
      method: "POST"
     }).then(function successCallback(response) {
      console.log(JSON.stringify(response.data));

      if(response.data.success){
        console.debug(JSON.stringify(response.data.user));
        $scope.user = response.data.user;
        window.localStorage['authentication'] = JSON.stringify(response.data.user);
        console.log(window.localStorage['authentication']);
        $scope.isLogged = true;
        $timeout(function() {
          $scope.modalLoading.hide();
          $scope.closeLogin();
        }, 1000);
      } else {
        $scope.messageLogin = response.data.message;
        $scope.successLogin = response.data.success;
        $scope.modalLoading.hide();
      }
    });
  };

  $scope.doSignup = function() {
    console.log('Doing SignUp', $scope.signUpData);

    $scope.modalLoading.show();

    console.log(JSON.stringify($scope.signUpData));
    var email = $scope.signUpData.username;
    var name = $scope.signUpData.name;
    var password = $scope.signUpData.password;

    console.log($rootScope.$urlWebsite);
    $http({
      url: $rootScope.$urlWebsite+'/api/auth/signup/'+email+'/'+password+'/'+name+'/', 
      method: "POST"
     }).then(function successCallback(response) {
      console.log(JSON.stringify(response.data));

      if(response.data.success){
        console.debug(JSON.stringify(response.data));

        $scope.user = response.data.user;
        window.localStorage['authentication'] = JSON.stringify(response.data.user);
        console.log(window.localStorage['authentication']);
        $scope.isLogged = true;
        $timeout(function() {
          $scope.modalLoading.hide();
          $scope.closeSignUp();
        }, 1000);
      } else {
        $scope.messageLogin = response.data.message;
        $scope.successLogin = response.data.success;
        $scope.modalLoading.hide();
      }
    });
  };

})



.controller('ValidateCtrl', function($scope, $http, $rootScope, $state) {

   $scope.validateData = {};
   $scope.doValidate = function() {
    console.log('Doing Validate', $scope.validateData);

    $scope.modalLoading.show();

    var userID = $scope.user._id;
    var devID = $scope.validateData.serial;

    console.log($rootScope.$urlWebsite);
    $http({
      url: $rootScope.$urlWebsite+'/api/dev/validate/'+devID+'/'+userID+'/', 
      method: "POST"
     }).then(function successCallback(response) {
      console.debug(JSON.stringify(response));

      if(response.data.success){
        
          $scope.modalLoading.hide();
          $scope.message = response.data.message;
          $state.go('app.single', {device: devID});
          $scope.successValidate = true;
       
      } else {
        
        $scope.successValidate = false;
        $scope.message = response.data.message;
        $scope.modalLoading.hide();
      }
    });
  };

})

.controller('homeCtrl', function($scope, $cordovaInAppBrowser, $rootScope) {

  var options = {
     location: 'yes',
     clearcache: 'yes',
     toolbar: 'yes'
  };

  $scope.openBrowser = function() {
     $cordovaInAppBrowser.open('http://192.168.4.1/wifi?', '_blank', options)
  
     .then(function(event) {
        console.log('Evento:' + event);
     })
  
     .catch(function(event) {
        // error

        console.log('Evento:' + event);
     });
  }

  
  // $scope.playlists = [
  //   { title: 'Reggae', id: 1 },
  //   { title: 'Chill', id: 2 },
  //   { title: 'Dubstep', id: 3 },
  //   { title: 'Indie', id: 4 },
  //   { title: 'Rap', id: 5 },
  //   { title: 'Cowbell', id: 6 }
  // ];
  
})

.controller('wifiConfigureCtrl', ['$scope', '$http','$state', function($scope, $http, $state){
  $scope.wifiData = {};

  $scope.submitWifiInfo = function() {
    var s = $scope.wifiData.ssid;
    var p = $scope.wifiData.password;

    $http({
      url: 'http://192.168.4.1/wifisave?s='+s+'&p='+p, 
      method: "GET"
     }).then(function successCallback(response) {

      console.log(JSON.stringify(response));

      if(response.statusText == 'OK'){
        $scope.statusConnection = true;
        $state.reload();
      }
    });

  };
}])

.controller('ChildCtrl', ['$scope', '$stateParams', '$http', '$rootScope', '$state', function ($scope, $stateParams, $http, $rootScope, $state) { 
  
  if(!$scope.isLogged){
    $state.go('app.home');
  } else {

    var device = $stateParams.device;
    console.debug(device);

    $scope.deviceData = {};

    $http({
      url: $rootScope.$urlWebsite+'/api/dev/'+device+'/', 
      method: "POST"
     }).then(function successCallback(response) {

      $scope.deviceData = {
        temperature: response.data.temperature,
        lightTimer: response.data.lightTimer,
        lightDuration: response.data.lightDuration,
       
        waterTimer: response.data.waterTimer
      };

      if(response.data.lightStatus == 1){
        $scope.deviceData.lightStatus = true;
      } else {
        $scope.deviceData.lightStatus = false;

      }

      console.debug('deviceData');
      console.debug(JSON.stringify($scope.deviceData));

    });
  }


  $scope.updateDevice = function() {
    console.log($scope.deviceData);
    console.log
    var lightStatus;

    if($scope.deviceData.lightStatus){
      lightStatus = 1;
    } else {
      lightStatus = 0;
    }

    $http({
      url: $rootScope.$urlWebsite+'/api/dev/update/'+device+'/'+$scope.deviceData.temperature+'/'+$scope.deviceData.lightTimer+'/'+$scope.deviceData.lightDuration+'/'+$scope.deviceData.waterTimer+'/'+lightStatus, 
      method: "POST"
     }).then(function successCallback(response) {
      
      if(response.data.success){
        $scope.successDevice = response.data.success;
        $scope.messageDevice = response.data.message;
      }

    });


  };

}])

.controller('DeviceCtrl', ['$scope', '$stateParams', function($scope, $http, $stateParams) {

  console.debug($stateParams);
  // var device = $stateParams.device;

  // $http({
  //     url: 'http://localhost:3000/api/dev/'+device+'/', 
  //     method: "POST"
  //    }).then(function successCallback(response) {

  //     console.log(JSON.stringify(response.data));

  //     if(response.data.success){
  //       $scope.devices = response.data.devices;
  //     } else {
  //       $scope.successDevice = 'Nenhum dispositivo criado';
  //     }

  //   });

}])

.controller('DevicesCtrl', function($scope, $http, $rootScope) {

  if($scope.isLogged){

    var user = JSON.parse(window.localStorage['authentication']);
    var userid = user._id;


    $http({
      url: $rootScope.$urlWebsite+'/api/dev/all/'+userid+'/', 
      method: "POST"
     }).then(function successCallback(response) {

      if(response.data.success){
        $scope.devices = response.data.devices;
      } else {
        $scope.successDevice = 'Nenhum dispositivo criado';
      }

    });
  
  } else {
    $state.go('app.home');
  }


})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
